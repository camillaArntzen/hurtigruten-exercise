
# Hurtigruten frontend exercise
For this exercise I created a search with functionality to search as you type

## Setup
<ul>
<li>Clone the repo</li>
<li> npm install </li>
<li> npm run server (To start the server)</li>
<li> npm start</li>
<li> Navigate to http://localhost:3000 </li>
</ul>

## Author
<p>Camilla Arntzen </p>

