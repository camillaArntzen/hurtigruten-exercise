const BASE_URL = "http://localhost:4000/api/ships/";
async function getShips(){
    const response = await fetch(BASE_URL).then(response => response.json());
    if(response){
        return response;
    }
}

async function getShip(name){
    const response = await fetch(BASE_URL + name).then(response => response.json());
    if(response){
        if(response.length === 0){
            alert('Cannot find ship :(');
        } else {
            return response;
        }
    }
}

export { getShips, getShip }