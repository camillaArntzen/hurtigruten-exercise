
const Results = ({results, searchValue}) => {

    const showResults = () => {
        return results.filter(res => res.name.toLowerCase().includes(searchValue.toLowerCase())).map((res) => {
            return (
                <div className="search-results">
                    <p className="search-results-text" key={res.name}>{res.name}</p>
                </div>
            )
        })
    }
    return (
        <div className="search-results-container">
            {results && showResults()}
        </div>
    )
}

export default Results;