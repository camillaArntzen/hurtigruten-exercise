//importing icons from assets folder
import Cross from '../assets/cross.svg';
import MagnifyingGlass from '../assets/magnifying-glass.svg';

const Search = ({ searchValue, setSearchValue, onEnter, handleFocus }) => {

    const handleSearch = () => {
        if(searchValue !== ''){
            setSearchValue('')
        }
    }

  return (
    <div className="search-container">
        <input 
            type="text"
            placeholder="Search"
            role="search"
            className="search-input"
            onChange={e => setSearchValue(e.target.value)}
            value={searchValue}
            onKeyPress={event => {
                if (event.key === 'Enter') {
                    //onEnter();
                }
            }}
            onFocus={handleFocus}
        />
        <img className="search-icon" src={searchValue !== '' ? Cross : MagnifyingGlass} alt="icon" onClick={handleSearch}/>
    </div>
  );
}

export default Search;
