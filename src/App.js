import { useState, useEffect } from 'react';

// api methods
import { getShips, getShip } from './api/ships-api'

// components
import Search from './components/Search'
import Results from './components/Results'

const App = () => {
    const [searchValue, setSearchValue] = useState('');
    const [results, setResults] = useState([]);

    useEffect(() => {
        getData();
    },[]);

    const getMatchingData = async () => {
        let res = await getShip(searchValue);
        setResults(res);
    }

    const getData = async () => {
        if(results.length <= 1){
            let res = await getShips();
            setResults(res);
        }
    }
  return (
    <div className="App">
        <main>
            <Search 
                searchValue={searchValue} 
                setSearchValue={setSearchValue} 
                onEnter={getMatchingData}
                handleFocus={getData}
            />
            <Results results={results} searchValue={searchValue} />
        </main>    
    </div>
  );
}

export default App;
